/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 13:58:28 by fdubois           #+#    #+#             */
/*   Updated: 2018/12/30 19:31:48 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		xytoindex(int x, int y)
{
	return ((IMG_X * y) + x);
}

int		mlx_keypress(void *win_ptr, int (*f)(), void *param)
{
	return (mlx_hook(win_ptr, KEYPRESS, KEYPRESSMASK, f, param));
}

void	current(t_map *map)
{
	while (0)
		map->proj[0][0].x = 0;
}

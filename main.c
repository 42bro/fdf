/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 17:07:29 by fdubois           #+#    #+#             */
/*   Updated: 2019/01/02 17:23:15 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	controls(int key, t_map *map)
{
	static void	(*proj)(t_map*);

	if (proj == NULL)
		proj = isoproj;
	if (key == KEY_UP || key == KEY_DOWN || key == KEY_LEFT || key == KEY_RIGHT)
		move_it(key, map, proj);
	else if (key == KEY_W || key == KEY_S)
		zoom_it(key, map, proj);
	else if (key == KEY_T || key == KEY_G)
		change_z_scale(key, map, proj);
	else if ((key == KEY_D || key == KEY_A))
		rotate_it(key, map, proj);
	else if (key == KEY_I || key == KEY_P)
	{
		if (key == KEY_I)
			proj = isoproj;
		else
			proj = paraproj;
		switch_proj(key, map, proj);
	}
	else if (key == KEY_ESCAPE)
		exit(0);
	return (0);
}

int			main(int ac, char **av)
{
	int		bpp;
	int		linesize;
	int		endian;
	t_map	*map;

	if (ac != 2)
		usage_msg();
	bpp = 32;
	linesize = ((bpp / 4) * IMG_X);
	endian = 1;
	if (!(map = readmap(open(av[1], O_RDONLY))))
		map_error(&map);
	init_params(map);
	malloc_struct(map);
	map->mlx_ptr = mlx_init();
	map->win_ptr = mlx_new_window(map->mlx_ptr, WIN_X, WIN_Y, "FdF");
	mlx_string_put(map->mlx_ptr, map->win_ptr,
	(IMG_X / 3), (IMG_Y + 1), 0xFFFFFF, CTRL);
	map->img = mlx_new_image(map->mlx_ptr, IMG_X, IMG_Y);
	map->img_ptr = (int*)mlx_get_data_addr(map->img, &bpp, &linesize, &endian);
	render(map, isoproj);
	mlx_keypress(map->win_ptr, controls, map);
	mlx_loop(map->mlx_ptr);
	free_it(&map);
	return (0);
}

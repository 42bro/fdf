/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 14:20:29 by fdubois           #+#    #+#             */
/*   Updated: 2018/12/30 19:24:50 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point	*vertical(t_point a[2])
{
	static t_point tmp[2];

	tmp[0] = a[0];
	tmp[1].x = a[1].x - 1;
	tmp[1].y = a[1].y + 1;
	return (tmp);
}

void	render(t_map *map, void (*proj)(t_map*))
{
	draw_it(map, proj);
	mlx_put_image_to_window(map->mlx_ptr, map->win_ptr, map->img, 0, 0);
}

void	clear_screen(t_map *map)
{
	erase_it(map, current);
	mlx_put_image_to_window(map->mlx_ptr, map->win_ptr, map->img, 0, 0);
}

void	draw_it(t_map *map, void (*f)(t_map*))
{
	t_point a[2];

	a[1].y = -1;
	f(map);
	while (++(a[1].y) < map->height)
	{
		a[1].x = 0;
		a[0].x = 0;
		a[0].y = a[1].y;
		while (++(a[1].x) < map->width)
		{
			drawline(a, map);
			if (a[1].y < map->height - 1)
				drawline(vertical(a), map);
			a[0].x = a[1].x;
		}
		if (a[1].y < map->height - 1)
			drawline(vertical(a), map);
	}
}

void	erase_it(t_map *map, void (*f)(t_map*))
{
	t_point a[2];

	a[1].y = -1;
	f(map);
	while (++(a[1].y) < map->height)
	{
		a[1].x = 0;
		a[0].x = 0;
		a[0].y = a[1].y;
		while (++(a[1].x) < map->width)
		{
			eraseline(a, map);
			if (a[1].y < map->height - 1)
				eraseline(vertical(a), map);
			a[0].x = a[1].x;
		}
		if (a[1].y < map->height - 1)
			eraseline(vertical(a), map);
	}
}

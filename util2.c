/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/27 20:32:48 by fdubois           #+#    #+#             */
/*   Updated: 2018/12/30 19:43:04 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		visible(int x, int y)
{
	if (x >= 0 && y >= 0 && x < IMG_X && y < IMG_Y)
		return (1);
	return (0);
}

t_map	*init_struct(void)
{
	t_map *map;

	if (!(map = (t_map*)malloc(sizeof(t_map))))
		return (NULL);
	ft_bzero(map, sizeof(t_map));
	if (!(map->tab = (int**)malloc(sizeof(int*) * MAX_MAP_HEIGHT)))
		return (NULL);
	return (map);
}

void	malloc_struct(t_map *map)
{
	int i;

	if (!(map->proj = ((t_point**)malloc(sizeof(t_point*) * map->height))))
		malloc_fail();
	i = -1;
	while (++i < map->height)
	{
		if (!(map->proj[i] = (t_point*)malloc(sizeof(t_point) * map->width)))
			malloc_fail();
	}
}

void	init_params(t_map *map)
{
	map->x_scale = 1;
	map->y_scale = 1;
	map->z_scale = 8;
	map->x_offset = 0;
	map->y_offset = 0;
	map->zoom_lvl = 1;
	map->z_rot = 0;
}

void	free_it(t_map **map)
{
	int i;

	i = -1;
	while (++i < (*map)->height)
	{
		free((*map)->tab[i]);
		(*map)->tab[i] = NULL;
		free((*map)->proj[i]);
		(*map)->proj[i] = NULL;
	}
	free((*map)->tab);
	(*map)->tab = NULL;
	free((*map)->proj);
	(*map)->proj = NULL;
	free((*map)->img_ptr);
	(*map)->img_ptr = NULL;
	free((*map)->img);
	(*map)->img = NULL;
	free((*map)->win_ptr);
	(*map)->win_ptr = NULL;
	free((*map)->mlx_ptr);
	(*map)->mlx_ptr = NULL;
	free(map);
	map = NULL;
}

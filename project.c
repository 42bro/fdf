/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   project.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/29 06:43:34 by fdubois           #+#    #+#             */
/*   Updated: 2019/01/02 17:15:01 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	paraproj(t_map *map)
{
	float	diag;

	if (!(map->x_offset) && !(map->y_offset))
	{
		diag = sqrt(pow(IMG_X, 2) + pow(IMG_Y, 2));
		if (map->height > map->width)
		{
			map->y_scale *= (diag / (map->height * 4));
			map->x_scale *= (map->y_scale);
		}
		else
		{
			map->x_scale *= (diag / (map->width * 4));
			map->y_scale *= map->x_scale;
		}
		map->x_offset += (IMG_X / 3);
		map->y_offset += (IMG_Y / 8);
	}
	parap(map);
}

void	parap(t_map *map)
{
	int i;
	int j;

	i = -1;
	while (++i < map->height)
	{
		j = -1;
		while (++j < map->width)
		{
			map->proj[i][j].x = (map->x_scale * j) + ((map->height - i) * (
map->x_scale / 2)) + map->x_offset;
			if (map->tab[i][j] >= 0)
				map->proj[i][j].y = (map->y_scale * i) - (
((float)map->tab[i][j] / map->max_z) * map->z_scale) + map->y_offset;
			else
				map->proj[i][j].y = (map->y_scale * i) + (
((float)map->tab[i][j] / map->min_z) * map->z_scale) + map->y_offset;
		}
	}
}

void	isoproj(t_map *map)
{
	float	diag;

	if (!(map->x_offset) && !(map->y_offset))
	{
		diag = sqrt(pow(IMG_X, 2) + pow(IMG_Y, 2));
		if (map->height > map->width)
		{
			map->y_scale *= (diag / (map->height * 4));
			map->x_scale *= (map->y_scale);
		}
		else
		{
			map->x_scale *= (diag / (map->width * 4));
			map->y_scale *= map->x_scale;
		}
		map->x_offset += (IMG_X / 2);
		map->y_offset += (IMG_Y / 8);
		map->z_rot = 0.7854;
	}
	isop(map);
}

void	isop(t_map *map)
{
	int i;
	int j;

	i = -1;
	while (++i < map->height)
	{
		j = -1;
		while (++j < map->width)
		{
			map->proj[i][j].x = (map->x_scale * j * cos(map->z_rot)) - (
map->y_scale * i * sin(map->z_rot)) + map->x_offset;
			if (map->tab[i][j] < 0)
				map->proj[i][j].y = ((map->y_scale * i * cos(map->z_rot)) + (
map->x_scale * j * sin(map->z_rot))) + ((map->z_scale * (
(float)map->tab[i][j] / map->min_z))) + map->y_offset;
			else
				map->proj[i][j].y = ((map->y_scale * i * cos(map->z_rot)) + (
map->x_scale * j * sin(map->z_rot))) - ((map->z_scale * (
(float)map->tab[i][j] / map->max_z))) + map->y_offset;
		}
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 17:08:42 by fdubois           #+#    #+#             */
/*   Updated: 2019/01/02 17:14:48 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	l_erp(int a, int b, float ratio)
{
	int f;

	f = (int)(256 * ratio);
	return ((((((a & 0x00FF00FF) * (256 - f)) +
	((b & 0x00FF00FF) * f)) >> 8) & 0x00FF00FF) | (((((a & 0xFF00FF00) *
	(256 - f)) + ((b & 0xFF00FF00) * f)) >> 8) & 0xFF00FF00));
}

static int	zrgb(float z, t_map *map)
{
	if (z == map->max_z)
		return (CT);
	else if (z >= (0.8 * map->max_z))
		return (l_erp(C4, CT, (z - 0.8 * map->max_z) / (0.2 * map->max_z)));
	else if (z >= (0.6 * map->max_z))
		return (l_erp(C3, C4, (z - 0.6 * map->max_z) / (0.2 * map->max_z)));
	else if (z >= (0.4 * map->max_z))
		return (l_erp(C2, C3, (z - 0.4 * map->max_z) / (0.2 * map->max_z)));
	else if (z >= (0.2 * map->max_z))
		return (l_erp(C1, C2, (z - 0.2 * map->max_z) / (0.2 * map->max_z)));
	else if (z >= 0)
		return (l_erp(CMID, C1, z / (0.2 * map->max_z)));
	else if (z >= (0.2 * map->min_z))
		return (l_erp(CMID, CL1, z / (0.2 * map->min_z)));
	else if (z >= (0.4 * map->min_z))
		return (l_erp(CL1, CL2, (z - 0.2 * map->min_z) / (0.2 * map->min_z)));
	else if (z >= (0.6 * map->min_z))
		return (l_erp(CL2, CL3, (z - 0.4 * map->min_z) / (0.2 * map->min_z)));
	else if (z >= (0.8 * map->min_z))
		return (l_erp(CL3, CL4, (z - 0.6 * map->min_z) / (0.2 * map->min_z)));
	else if (z > map->min_z)
		return (l_erp(CL4, CLOW, (z - 0.8 * map->min_z) / map->min_z));
	return (CLOW);
}

void		piximg(int x, int y, t_point a[2], t_map *map)
{
	map->img_ptr[xytoindex(x, y)] = zrgb((map->tab[a[0].y][a[0].x] + (1 - (sqrt
(pow((map->proj[a[1].y][a[1].x].y - y), 2) + pow((map->proj[a[1].y][a[1].x]
	.x - x), 2)) / sqrt(pow((map->proj[a[1].y][a[1].x].y - map->proj[a[0].y]
	[a[0].x].y), 2) + pow((map->proj[a[1].y][a[1].x].x - map->proj[a[0].y]
	[a[0].x].x), 2)))) * (map->tab[a[1].y][a[1].x] - map->tab
	[a[0].y][a[0].x])), map);
}

void		drawline(t_point a[2], t_map *map)
{
	int	e;
	int	x0;
	int	y0;

	x0 = map->proj[a[0].y][a[0].x].x;
	y0 = map->proj[a[0].y][a[0].x].y;
	e = (abs(map->proj[a[1].y][a[1].x].x - x0) >
abs(map->proj[a[1].y][a[1].x].y - y0) ? (abs(map->proj[a[1].y][a[1].x].x - x0))
	: -(abs(map->proj[a[1].y][a[1].x].y - y0))) / 2;
	while (x0 != map->proj[a[1].y][a[1].x].x
	|| y0 != map->proj[a[1].y][a[1].x].y)
	{
		if (visible(x0, y0))
			piximg(x0, y0, a, map);
		if (e > -abs(map->proj[a[1].y][a[1].x].x - map->proj[a[0].y][a[0].x].x))
		{
			e -= abs(map->proj[a[1].y][a[1].x].y - map->proj[a[0].y][a[0].x].y);
			x0 += (x0 < map->proj[a[1].y][a[1].x].x ? 1 : -1);
		}
		if (e < abs(map->proj[a[1].y][a[1].x].y - map->proj[a[0].y][a[0].x].y))
		{
			e += abs(map->proj[a[1].y][a[1].x].x - map->proj[a[0].y][a[0].x].x);
			y0 += (y0 < map->proj[a[1].y][a[1].x].y ? 1 : -1);
		}
	}
}

void		eraseline(t_point a[2], t_map *map)
{
	int	e;
	int	x0;
	int	y0;

	x0 = map->proj[a[0].y][a[0].x].x;
	y0 = map->proj[a[0].y][a[0].x].y;
	e = (abs(map->proj[a[1].y][a[1].x].x - x0) >
abs(map->proj[a[1].y][a[1].x].y - y0) ? (abs(map->proj[a[1].y][a[1].x].x - x0))
	: -(abs(map->proj[a[1].y][a[1].x].y - y0))) / 2;
	while (x0 != map->proj[a[1].y][a[1].x].x
	|| y0 != map->proj[a[1].y][a[1].x].y)
	{
		if (visible(x0, y0))
			map->img_ptr[xytoindex(x0, y0)] = 0;
		if (e > -abs(map->proj[a[1].y][a[1].x].x - map->proj[a[0].y][a[0].x].x))
		{
			e -= abs(map->proj[a[1].y][a[1].x].y - map->proj[a[0].y][a[0].x].y);
			x0 += (x0 < map->proj[a[1].y][a[1].x].x ? 1 : -1);
		}
		if (e < abs(map->proj[a[1].y][a[1].x].y - map->proj[a[0].y][a[0].x].y))
		{
			e += abs(map->proj[a[1].y][a[1].x].x - map->proj[a[0].y][a[0].x].x);
			y0 += (y0 < map->proj[a[1].y][a[1].x].y ? 1 : -1);
		}
	}
}

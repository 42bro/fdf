/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   msg.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 15:55:22 by fdubois           #+#    #+#             */
/*   Updated: 2019/01/02 17:14:01 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	map_error(t_map **map)
{
	ft_putendl("invalid map file !");
	free((*map)->tab);
	(*map)->tab = NULL;
	free(*map);
	(*map) = NULL;
	exit(1);
}

void	map_too_big(t_map **map)
{
	ft_putendl("map too big !");
	free((*map)->tab);
	(*map)->tab = NULL;
	free(*map);
	(*map) = NULL;
	exit(1);
}

void	usage_msg(void)
{
	ft_putendl("usage : ./fdf [map file]");
	exit(1);
}

void	malloc_fail(void)
{
	ft_putendl("gros fail de malloc!");
	exit(1);
}

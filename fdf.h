/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 11:59:00 by fdubois           #+#    #+#             */
/*   Updated: 2019/01/02 17:09:02 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "libft/libft.h"
# include <fcntl.h>
# include "mlx/mlx.h"
# include "events.h"
# include <math.h>

# define CTRL "Move:Arrows Zoom:W/S Rotate(iso):A/D ViewSwitch:I/P AdjustZ:T/G"

# define IMG_X 2560
# define IMG_Y 1280
# define WIN_X 2560
# define WIN_Y 1440
# define MAX_MAP_HEIGHT 8192

# define CT 0x900000
# define C4 0xD20000
# define C3 0xFFFF00
# define C2 0x00FF00
# define C1 0x00FF55
# define CMID 0x00FFFF
# define CL1 0x00C5FF
# define CL2 0x0078FF
# define CL3 0x0000FF
# define CL4 0x650387
# define CLOW 0x021D42

typedef struct	s_l
{
	struct s_l	*next;
	char		*str;
}				t_l;

typedef struct	s_point
{
	int			x;
	int			y;
}				t_point;

typedef struct	s_map
{
	t_point		**proj;
	int			**tab;
	void		*img;
	int			*img_ptr;
	void		*win_ptr;
	void		*mlx_ptr;
	float		max_z;
	float		min_z;
	float		x_scale;
	float		y_scale;
	float		z_scale;
	float		z_rot;
	int			width;
	int			height;
	int			x_offset;
	int			y_offset;
	int			zoom_lvl;
}				t_map;

void			map_error(t_map **map);
void			map_too_big(t_map **map);
void			usage_msg(void);
void			malloc_fail(void);

t_map			*init_struct(void);
void			init_params(t_map *map);
void			malloc_struct(t_map *map);
void			print_controls(t_map *map);
void			free_it(t_map **map);
t_map			*readmap(int fd);

void			isoproj(t_map *map);
void			isop(t_map *map);
void			paraproj(t_map *map);
void			parap(t_map *map);

int				xytoindex(int x, int y);
int				visible(int x, int y);

t_point			*vertical(t_point a[2]);
void			drawline(t_point a[2], t_map *map);
void			eraseline(t_point a[2], t_map *map);
void			draw_it(t_map *map, void (*f)(t_map*));
void			erase_it(t_map *map, void (*f)(t_map*));
void			clear_screen(t_map *map);
void			current(t_map *map);
void			render(t_map *map, void (*proj)(t_map*));

int				mlx_keypress(void *win_ptr, int (*f)(), void *param);
void			switch_proj(int key, t_map *map, void (*proj)(t_map*));
void			move_it(int key, t_map *map, void (*proj)(t_map*));
void			zoom_it(int key, t_map *map, void (*proj)(t_map*));
void			rotate_it(int key, t_map *map, void (*proj)(t_map*));
void			change_z_scale(int key, t_map *map, void (*proj)(t_map*));

#endif

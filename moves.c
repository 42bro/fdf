/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moves.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 12:14:24 by fdubois           #+#    #+#             */
/*   Updated: 2019/01/02 17:27:40 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	switch_proj(int key, t_map *map, void (*proj)(t_map*))
{
	clear_screen(map);
	init_params(map);
	if (key == KEY_I)
		render(map, proj);
	else
		render(map, proj);
}

void	move_it(int key, t_map *map, void (*proj)(t_map*))
{
	clear_screen(map);
	if (key == KEY_UP)
		map->y_offset += 42;
	else if (key == KEY_LEFT)
		map->x_offset += 42;
	else if (key == KEY_DOWN)
		map->y_offset -= 42;
	else if (key == KEY_RIGHT)
		map->x_offset -= 42;
	render(map, proj);
}

void	zoom_it(int key, t_map *map, void (*proj)(t_map*))
{
	clear_screen(map);
	if (key == KEY_W)
	{
		map->zoom_lvl++;
		map->x_scale *= 1.5;
		map->y_scale *= 1.5;
		map->z_scale *= 1.5;
		map->y_offset -= (map->y_scale * map->height / 2);
	}
	else if (map->zoom_lvl != 0)
	{
		map->y_offset += (map->y_scale * map->height / 2);
		map->x_scale /= 1.5;
		map->y_scale /= 1.5;
		map->z_scale /= 1.5;
		map->zoom_lvl--;
	}
	render(map, proj);
}

void	rotate_it(int key, t_map *map, void (*proj)(t_map*))
{
	clear_screen(map);
	if (key == KEY_D)
		map->z_rot += 0.0174533;
	else if (key == KEY_A)
		map->z_rot -= 0.0174533;
	render(map, proj);
}

void	change_z_scale(int key, t_map *map, void (*proj)(t_map*))
{
	clear_screen(map);
	if (key == KEY_T)
		map->z_scale += 2;
	else if (map->z_scale > 0)
		map->z_scale -= 2;
	render(map, proj);
}

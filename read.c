/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 10:57:18 by fdubois           #+#    #+#             */
/*   Updated: 2019/01/02 17:21:00 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		ft_tablen(char **tab)
{
	int i;

	i = 0;
	if (tab)
	{
		while (tab[i] != NULL)
			i++;
		return (i);
	}
	else
		return (1);
}

void	parse(t_map *map, char *line, int i)
{
	char	**spl;
	int		j;
	int		k;

	j = -1;
	if (i == MAX_MAP_HEIGHT)
		map_too_big(&map);
	if (!(spl = ft_strsplit(line, ' ')))
		malloc_fail();
	if (i && ft_tablen(spl) != map->width)
		map_error(&map);
	map->width = ft_tablen(spl);
	if (!(map->tab[i] = (int*)malloc(sizeof(int) * map->width)))
		malloc_fail();
	k = -1;
	while (++k < map->width)
	{
		map->tab[i][++j] = ft_atoi(spl[k]);
		free(spl[k]);
		if ((float)map->tab[i][j] > map->max_z)
			map->max_z = (float)map->tab[i][j];
		else if ((float)map->tab[i][j] < map->min_z)
			map->min_z = (float)map->tab[i][j];
	}
	free(spl);
}

t_map	*readmap(int fd)
{
	int		i;
	char	*line;
	t_map	*map;

	i = 0;
	if (!(map = init_struct()))
		return (NULL);
	while (get_next_line(fd, &line) == 1)
	{
		parse(map, line, i);
		free(line);
		i++;
	}
	close(fd);
	if (!map->width || !i)
		map_error(&map);
	if (map->max_z == 0)
		map->max_z++;
	if (map->min_z == 0)
		map->min_z--;
	free(line);
	map->height = i;
	return (map);
}

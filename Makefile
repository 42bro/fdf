# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fdubois <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/28 16:32:15 by fdubois           #+#    #+#              #
#    Updated: 2019/01/02 17:21:53 by fdubois          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf
CC = gcc
FLAGS = -Wall -Wextra -Werror
SRCS = msg.c util.c util2.c read.c project.c moves.c main.c draw.c render.c
HEADERS = fdf.h events.h
OBJS = $(SRCS:.c=.o)
LIB = libft/

all: libs $(NAME)

libs:
		make -C $(LIB)
		make -C mlx/

$(NAME): $(OBJS)
		$(CC) $(FLAGS) $(OBJS) -L $(LIB) -lft -L mlx/ -lmlx -framework OpenGL -framework AppKit -o $(NAME)

%.o: %.c $(HEADERS)
	$(CC) $(FLAGS) -I. -o $@ -c $<

clean:
		make -C $(LIB) fclean
		make -C mlx/ clean
		rm -f $(OBJS)

fclean: clean
		rm -f $(NAME)

.PHONY: all libs clean fclean re

re: fclean all
